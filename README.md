![logo](https://github.com/blink7/Contacts/blob/master/src/main/webapp/resources/images/icon.png) Contacts
===========

**Contacts** is a simple web app with ability to manage contacts (CRUD), send them email, attach files. The app excludes IoC, MVC and ORM frameworks, just plain Servlets and JDBC.

## How To
1. Create MySQL Schema based on [ContactsDB.sql](https://github.com/blink7/Contacts/blob/master/ContactsDB.sql) file.

2. Fill the schema by [FillContactsDB.sql](https://github.com/blink7/Contacts/blob/master/FillContactsDB.sql).

3. Set DB connection through [context.xml](https://github.com/blink7/Contacts/blob/master/src/main/webapp/META-INF/context.xml):
```xml
  url="jdbc:mysql://localhost:3306/blink7_contacts?zeroDateTimeBehavior=convertToNull&amp;"
  username="user"
  password="password"
```
>  Don't forget to specify `zeroDateTimeBehavior=convertToNull&amp;` in url.
 
4. Set storage path on your local machine by setting [storage.properties](https://github.com/blink7/Contacts/blob/master/src/main/resources/storage.properties) to store contact attachments.

5. Configure mail properties by [mail.properties](https://github.com/blink7/Contacts/blob/master/src/main/resources/mail.properties):
```
  mail.from=mail@mail.com
  mail.user=mail@mail.com
  mail.password=password
  mail.admin=mail@mail.com
```
> Property `mail.admin` is sysadmin address for birthday alerting.

6. For logging set [log4j.properties](https://github.com/blink7/Contacts/blob/master/src/main/resources/log4j.properties).

7. Run the app by executing Gradle task:
```
  gradlew war
  gradlew tomcatRunWar
```
