package com.itechart.contacts.util;

import com.itechart.contacts.db.model.ContactModel;
import org.junit.Test;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TemplateTests {

    @Test
    public void renderTemplatesTest() {
        String text1 = "Happy Birthday, Test1! I hope your special day will bring you lots of happiness, " +
                "love and fun. You deserve them a lot. Enjoy!";
        String text2 = "May all your wishes get fulfilled in [Year]. Happy New Year!";

        STGroup group = new STGroupFile("templates/email_msg.stg");

        ST st1 = group.getInstanceOf("birthday");
        st1.add("contact", new ContactModel("Test1", "Test2"));
        String resultText1 = st1.render();
        assertNotNull(resultText1);
        assertEquals(text1, resultText1);
        System.out.println(resultText1);

        ST st2 = group.getInstanceOf("new_year");
        String resultText2 = st2.render();
        assertNotNull(resultText2);
        assertEquals(text2, resultText2);
        System.out.println(resultText2);
    }

    @Test
    public void getAdminMsgForBirthdays() {List<ContactModel> contacts = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            contacts.add(new ContactModel("Test" + i, "Test" + i));
        }

        STGroup group = new STGroupFile("templates/admin_mail_msg.stg");
        ST st = group.getInstanceOf("birthdays");
        st.add("contacts", contacts);
        String text = st.render();
        assertNotNull(text);
        System.out.println(text);
    }
}