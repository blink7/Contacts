package com.itechart.contacts.service;

import com.itechart.contacts.db.dao.AddressDao;
import com.itechart.contacts.db.dao.AttachmentDao;
import com.itechart.contacts.db.dao.ContactDao;
import com.itechart.contacts.db.dao.PhoneDao;
import com.itechart.contacts.db.model.*;
import com.itechart.contacts.db.transaction.MockTransactionManager;
import com.itechart.contacts.db.transaction.TransactionManager;
import com.itechart.contacts.storage.FileStorage;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

import static com.itechart.contacts.service.ContactsService.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ContactsService.class)
public class ContactsServiceTests {

    private static final int CONTACT_ID = 0;
    private static final int PAGE_1 = 1;

    private ContactsService service;

    @Mock
    private ServletContext context;
    @Mock
    private ContactDao contactDao;
    @Mock
    private AddressDao addressDao;
    @Mock
    private PhoneDao phoneDao;
    @Mock
    private AttachmentDao attachmentDao;
    @Mock
    private FileStorage storage;

    @Mock
    private HttpServletRequest req;
    @Mock
    private HttpServletResponse resp;
    @Mock
    private RequestDispatcher reqDispatcher;
    @Mock
    private HttpSession session;

    @Mock
    private ServletFileUpload upload;
    @Mock
    private FileItem fileItem1;
    @Mock
    private FileItem fileItem2;

    private TransactionManager txManager = new MockTransactionManager();

    private Map<String, Object> attributes = new HashMap<>();
    private List<ContactModel> contacts = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        service = new ContactsService(context, txManager, contactDao, addressDao, attachmentDao, phoneDao, storage);

        attributes.clear();
        doAnswer(invocation -> {
            attributes.put(invocation.getArgument(0), invocation.getArgument(1));
            return null;
        }).when(req).setAttribute(anyString(), any());

        contacts.clear();
        for (int i = 0; i < 10; i++) {
            contacts.add(new ContactModel("Gag", "Fag"));
        }

        when(req.getSession()).thenReturn(session);
        when(req.getRequestDispatcher(LIST)).thenReturn(reqDispatcher);
        when(req.getRequestDispatcher(EDIT)).thenReturn(reqDispatcher);

        when(fileItem1.isFormField()).thenReturn(true);
        when(fileItem2.isFormField()).thenReturn(false);
        when(fileItem2.getFieldName()).thenReturn("");
    }

    @Test
    public void loadContactsListTest() throws Exception {
        when(req.getParameter("page")).thenReturn(String.valueOf(PAGE_1));
        when(session.getAttribute(CURRENT_PAGE_KEY)).thenReturn(PAGE_1);

        when(contactDao.getContactsByPage(PAGE_1, CONTACTS_AMOUNT_PER_PAGE)).thenReturn(contacts);

        service.loadContactsList(req, resp);

        verify(reqDispatcher).forward(req, resp);
        assertNotNull(attributes.get("contacts"));
        assertEquals(contacts, attributes.get("contacts"));
    }

    @Test
    public void loadContactTest() throws Exception {
        ContactModel contact = new ContactModel("Gag", "Fag");
        List<PhoneModel> phones = Collections.singletonList(new PhoneModel());
        List<AttachmentModel> attachments = Collections.singletonList(new AttachmentModel());
        contact.setPhones(phones);
        contact.setAttachments(attachments);

        when(req.getParameter("id")).thenReturn(String.valueOf(CONTACT_ID));

        when(contactDao.get(CONTACT_ID)).thenReturn(contact);
        when(phoneDao.get(CONTACT_ID)).thenReturn(phones);
        when(attachmentDao.get(CONTACT_ID)).thenReturn(attachments);

        service.loadContact(req, resp);

        verify(reqDispatcher).forward(req, resp);
        assertNotNull(attributes.get("forEdit"));
        assertEquals(true, attributes.get("forEdit"));
        assertNotNull(attributes.get("contact"));
        assertEquals(contact, attributes.get("contact"));
    }

    @Test
    public void deleteContactTest() throws Exception {
        ContactModel contact = new ContactModel("Gag", "Fag");
        contact.setId(CONTACT_ID);
        AddressModel address1 = new AddressModel();
        address1.setId(0);
        contact.setAddress(address1);

        when(req.getParameter("ids")).thenReturn("[" + CONTACT_ID + "]");
        when(contactDao.get(CONTACT_ID)).thenReturn(contact);

        service.deleteContact(req, resp);

        verify(resp).sendRedirect("/contacts");
        verify(addressDao).delete(address1.getId());
        verify(contactDao).delete(CONTACT_ID);
    }

    @Test
    @SuppressWarnings("all")
    public void saveContactTest() throws Exception {
        File file = File.createTempFile("gag_", "_fag");
        ContactModel[] contactResult = new ContactModel[1];

        when(context.getAttribute("javax.servlet.context.tempdir")).thenReturn(mock(File.class));
        whenNew(ServletFileUpload.class).withAnyArguments().thenReturn(upload);
        when(upload.parseRequest(req)).thenReturn(Arrays.asList(fileItem1, fileItem2));
        when(fileItem1.getFieldName()).thenReturn("contact_json");
        when(fileItem1.getString(anyString()))
                .thenReturn("{\"first_name\":\"Gag\",\"last_name\":\"Fag\",\"address\":{}," +
                        "\"phones\":[{\"phone_id\":\"-1\",\"country_code\":\"333\",\"operator_code\":\"22\"," +
                        "\"phone_num\":\"1111111\",\"phone_type\":\"home\"," +
                        "\"comment\":\"My gag cell phone, take it all!\",\"mark\":\"is-new\"}]," +
                        "\"attachments\":[{\"attachment_id\":\"-1\",\"comment\":\"My gag zip, take it all!\"," +
                        "\"mark\":\"is-new\"}],\"contact_id\":\"0\"}");
        when(fileItem2.getFieldName()).thenReturn("newAttachment-1");
        when(fileItem2.getInputStream()).thenReturn(new FileInputStream(file));
        when(fileItem2.getName()).thenReturn("fakeFileName");
        when(storage.saveFile(eq(CONTACT_ID), any(InputStream.class))).thenReturn("fakeUuid");

        when(contactDao.add(any(ContactModel.class))).thenAnswer(invocation -> {
            contactResult[0] = invocation.getArgument(0);
            return null;
        }).thenReturn(CONTACT_ID);
        when(contactDao.update(any(ContactModel.class))).thenAnswer(invocation -> {
            ContactModel contact = invocation.getArgument(0);
            assertEquals(CONTACT_ID, contact.getId());
            return null;
        });
        when(attachmentDao.add(any(AttachmentModel.class))).thenAnswer(invocation -> {
            contactResult[0].setAttachments(Collections.singletonList(invocation.getArgument(0)));
            return null;
        });
        when(phoneDao.add(any(PhoneModel.class))).thenAnswer(invocation -> {
            contactResult[0].setPhones(Collections.singletonList(invocation.getArgument(0)));
            return null;
        });

        service.saveContact(req, resp);

        assertTrue(contactResult.length != 0);
        ContactModel savedContact = contactResult[0];
        assertEquals(savedContact.getFirstName(), "Gag");
        assertEquals(savedContact.getLastName(), "Fag");
        AttachmentModel savedAttachment = savedContact.getAttachments().get(0);
        assertEquals("fakeUuid", savedAttachment.getFileUuid());
        assertEquals("fakeFileName", savedAttachment.getFileName());
        assertEquals("My gag zip, take it all!", savedAttachment.getComment());
        PhoneModel savedPhone = savedContact.getPhones().get(0);
        assertEquals("333-22-1111111", savedPhone.getFullPhoneNumber());
        verify(resp).sendRedirect("/contacts");

        file.delete();
    }

    @Test
    public void searchContactsTest() throws Exception {
        SearchCriteria criteria = new SearchCriteria();
        criteria.setFirstName("Gag");
        criteria.setLastName("Fag");

        when(req.getParameter("criteria"))
                .thenReturn("{\"address\":{},\"first_name\":\"Gag\",\"last_name\":\"Fag\"}");
        when(req.getParameter("page")).thenReturn(String.valueOf(PAGE_1));
        when(session.getAttribute(CURRENT_SEARCH_PAGE_KEY)).thenReturn(PAGE_1);

        when(contactDao.getContactsBySearch(eq(PAGE_1), eq(CONTACTS_AMOUNT_PER_PAGE), any(SearchCriteria.class)))
                .thenReturn(contacts);

        service.searchContacts(req, resp);

        verify(reqDispatcher).forward(req, resp);
        assertNotNull(attributes.get("contacts"));
        assertEquals(contacts, attributes.get("contacts"));
    }
}