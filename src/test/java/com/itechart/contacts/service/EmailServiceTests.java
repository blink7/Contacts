package com.itechart.contacts.service;

import com.itechart.contacts.db.dao.ContactDao;
import com.itechart.contacts.db.dao.MailDao;
import com.itechart.contacts.db.model.ContactModel;
import com.itechart.contacts.db.model.MailModel;
import com.itechart.contacts.db.transaction.MockTransactionManager;
import com.itechart.contacts.db.transaction.TransactionManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneOffset;

import static com.itechart.contacts.service.ContactsService.ACTION_MESSAGE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmailServiceTests {

    private static final int CONTACT_ID = 0;

    private EmailService service;

    @Mock
    private ContactDao contactDao;
    @Mock
    private MailDao mailDao;

    @Mock
    private HttpServletRequest req;
    @Mock
    private HttpServletResponse resp;
    @Mock
    private HttpSession session;

    private TransactionManager txManager = new MockTransactionManager();

    @Before
    public void setUp() throws Exception {
        service = new EmailService(txManager, contactDao, mailDao);
    }

    @Test
    public void sendMailTest() throws Exception {
        ContactModel contact = new ContactModel("Gag", "Fag");
        contact.setId(CONTACT_ID);
        contact.setEmail("test@mail.com");
        contact.setBirthday(new Date(LocalDate
                .of(1993, 6, 29)
                .atStartOfDay(ZoneOffset.UTC)
                .toInstant()
                .toEpochMilli()
        ));

        when(req.getParameter("content"))
                .thenReturn("{\"recipients\":[\"0\"],\"subject\":\"Some subject\",\"template\":\"birthday\"}");
        when(contactDao.get(CONTACT_ID)).thenReturn(contact);
        MailModel[] mailResult = new MailModel[1];
        when(mailDao.archiveMail(any(MailModel.class))).then(invocation -> {
            mailResult[0] = invocation.getArgument(0);
            return null;
        });
        when(req.getSession()).thenReturn(session);

        service.sendMail(req, resp);

        assertTrue(mailResult.length != 0);
        assertEquals(CONTACT_ID, mailResult[0].getContactId());
        assertEquals("Some subject", mailResult[0].getSubject());
        verify(session).setAttribute(eq(ACTION_MESSAGE), anyString());
        verify(resp).sendRedirect("/contacts");
    }
}