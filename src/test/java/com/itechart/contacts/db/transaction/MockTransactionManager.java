package com.itechart.contacts.db.transaction;

import com.itechart.contacts.exception.TransactionException;

public class MockTransactionManager implements TransactionManager {

    @Override
    public <T> T executeTransaction(TransactionConsumer<T> transaction) throws TransactionException {
        return transaction.execute();
    }
}