package com.itechart.contacts.util;

import com.google.gson.*;
import com.itechart.contacts.db.model.ContactModel;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class Utils {

    public static Gson buildGsonInstance() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("MM/dd/yyyy");
        /*gsonBuilder.registerTypeAdapter(Date.class, (JsonDeserializer<Date>) (json, typeOfT, context) -> {
            try {
                DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                return new Date(df.parse(json.getAsString()).getTime());
            } catch (ParseException e) {
                return null;
            }
        });
        gsonBuilder.registerTypeAdapter(Timestamp.class, (JsonDeserializer<Timestamp>) (json, typeOfT, context) -> {
            try {
                DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                return new Timestamp(df.parse(json.getAsString()).getTime());
            } catch (ParseException e) {
                return null;
            }
        });*/
        return gsonBuilder.create();
    }

    public static <T> T requireNonNull(T object, String message) {
        if (object == null) {
            throw new NullPointerException(message);
        }
        return object;
    }

    public static String getMessage(String key, Locale locale) {
        ResourceBundle resource = ResourceBundle.getBundle(
                "locale/messages",
                locale != null ? locale : Locale.getDefault());
        return resource.getString(key);
    }
}