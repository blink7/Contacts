package com.itechart.contacts.util;

import com.itechart.contacts.db.model.ContactModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TemplateHelper {

    private static final Logger log = LoggerFactory.getLogger(TemplateHelper.class);

    public static String getRenderedMsg(String templateName, ContactModel contact) {
        STGroup group = new STGroupFile("templates/email_msg.stg");
        ST st = group.getInstanceOf(templateName);
        if ("birthday".equalsIgnoreCase(templateName)) {
            st.add("contact", contact);
            return st.render();
        } else if ("new_year".equalsIgnoreCase(templateName)) {
            st.add("year", LocalDate.now().getYear());
            return st.render();
        }
        return null;
    }

    public static Map<String, String> getAllMsgs() {
        STGroup group = new STGroupFile("templates/email_msg.stg");
        Set<String> templateNames = group.getTemplateNames();
        Map<String, String> templates = new HashMap<>(templateNames.size());
        for (String templateName : templateNames) {
            if ("/birthday".equalsIgnoreCase(templateName)) {
                ST st = group.getInstanceOf(templateName);
                st.add("contact", new ContactModel(null, null));
                templates.put("birthday", st.render());
            } else if ("/new_year".equalsIgnoreCase(templateName)) {
                ST st = group.getInstanceOf(templateName);
                templates.put("new_year", st.render());
            }
            log.debug("Loaded template with name: {}", templateName);
        }
        return templates;
    }

    public static String getAdminMsg(String templateName, List<ContactModel> contacts) {
        STGroup group = new STGroupFile("templates/admin_mail_msg.stg");
        ST st = group.getInstanceOf(templateName);
        if ("birthdays".equalsIgnoreCase(templateName)) {
            st.add("contacts", contacts);
            return st.render();
        }
        return null;
    }
}