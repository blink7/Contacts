package com.itechart.contacts.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.itechart.contacts.service.ContactsService.CURRENT_PAGE_KEY;
import static com.itechart.contacts.service.ContactsService.CURRENT_SEARCH_PAGE_KEY;

@WebFilter(servletNames = {"ContactsController"})
public class ContactsFilter implements Filter {

    private static final Logger log = LoggerFactory.getLogger(ContactsFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // NOP
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        req.setCharacterEncoding("UTF-8");

        String[] pathParts = req.getRequestURI().split("/");
        if (pathParts.length == 2 && pathParts[1].equalsIgnoreCase("contacts")) {
            String page = req.getParameter("page");
            if (page != null && !validateNumber(page)) {
                log.warn("The page number '{}' is not an integer", page);
                resp.sendRedirect("/contacts");
                return;
            } else if (page != null && validateNumber(page) && Integer.parseInt(page) < 1) {
                log.warn("The page number '{}' is less than one", page);
                resp.sendRedirect("/contacts");
                return;
            }
        } else if (pathParts.length == 3 && pathParts[2].equalsIgnoreCase("edit")) {
            String id = req.getParameter("id");
            if (id == null || !validateNumber(id)) {
                log.warn("Invalid contact id '{}'", id);
                resp.sendRedirect("/contacts");
                return;
            }
        }

        if (req.getSession().getAttribute(CURRENT_PAGE_KEY) == null) {
            req.getSession().setAttribute(CURRENT_PAGE_KEY, 1);
        }
        if (req.getSession().getAttribute(CURRENT_SEARCH_PAGE_KEY) == null) {
            req.getSession().setAttribute(CURRENT_SEARCH_PAGE_KEY, 1);
        }

        chain.doFilter(request, response);
    }

    private boolean validateNumber(String number) {
        Matcher matcher = Pattern.compile("^\\d+$").matcher(number);
        return matcher.find();
    }

    @Override
    public void destroy() {
        // NOP
    }
}