package com.itechart.contacts.db.dao;

import com.itechart.contacts.db.model.MailModel;
import com.itechart.contacts.exception.TransactionException;

public interface MailDao {

    int archiveMail(MailModel mail) throws TransactionException;
}