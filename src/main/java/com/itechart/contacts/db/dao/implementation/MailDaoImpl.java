package com.itechart.contacts.db.dao.implementation;

import com.itechart.contacts.db.dao.MailDao;
import com.itechart.contacts.db.model.MailModel;
import com.itechart.contacts.exception.TransactionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.itechart.contacts.util.JdbcUtils.closeQuietly;

public class MailDaoImpl implements MailDao {

    private static final Logger log = LoggerFactory.getLogger(MailDaoImpl.class);

    private static final String INSERT_SQL
            = "INSERT INTO mail(contact_id, mail_subject, mail_text, send_date) VALUES (?, ?, ?, ?)";

    private DataSource dataSource;

    public MailDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public int archiveMail(MailModel mail) throws TransactionException {
        log.info("Archive the sent mail");

        PreparedStatement stmt = null;
        ResultSet keyRs = null;
        try {
            Connection connection = dataSource.getConnection();
            stmt = connection.prepareStatement(INSERT_SQL);
            stmt.setInt(1, mail.getContactId());
            stmt.setString(2, mail.getSubject());
            stmt.setString(3, mail.getText());
            stmt.setDate(4, mail.getSendDate());
            return stmt.executeUpdate();
        } catch (SQLException e) {
            throw new TransactionException("Archive Mail exception", e);
        } finally {
            closeQuietly(keyRs, stmt);
        }
    }
}