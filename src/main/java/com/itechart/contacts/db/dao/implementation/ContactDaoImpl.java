package com.itechart.contacts.db.dao.implementation;

import com.itechart.contacts.db.dao.ContactDao;
import com.itechart.contacts.db.model.AddressModel;
import com.itechart.contacts.db.model.ContactModel;
import com.itechart.contacts.db.model.SearchCriteria;
import com.itechart.contacts.exception.TransactionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static com.itechart.contacts.util.JdbcUtils.*;

public class ContactDaoImpl implements ContactDao {

    private static final Logger log = LoggerFactory.getLogger(ContactDaoImpl.class);

    private static final String INSERT_SQL
            = "INSERT INTO contact(f_name, l_name, m_name, birthday, sex, nationality, " +
            "marital_status, site, email, current_job, address_id, avatar) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    private static final String UPDATE_SQL
            = "UPDATE contact SET f_name = ?, l_name = ?, m_name = ?, birthday = ?, " +
            "sex = ?, nationality = ?, marital_status = ?, site = ?, email = ?, " +
            "current_job = ?, address_id = ?, avatar = ? WHERE id = ?";

    private static final String SELECT_SQL
            = "SELECT contact.*, address.* FROM contact LEFT JOIN address " +
            "ON contact.address_id = address.id WHERE contact.id = ?";

    private static final String SELECT_ALL_SQL
            = "SELECT contact.id, contact.f_name, contact.l_name, contact.m_name, " +
            "contact.birthday, contact.current_job, contact.email, address.country, address.city, " +
            "address.detail_address, address.zip FROM contact LEFT JOIN address " +
            "ON contact.address_id = address.id WHERE contact.delete_flag IS NOT TRUE LIMIT ? OFFSET ?";

    private static final String SELECT_ALL_BIRTHDAY_SQL
            = "SELECT contact.id, contact.f_name, contact.l_name, " +
            "contact.birthday, contact.email FROM contact " +
            "WHERE DATE_FORMAT(contact.birthday, '%m-%d') = DATE_FORMAT(?, '%m-%d')";

    private static final String DELETE_SQL = "UPDATE contact SET delete_flag = 1, delete_date = ? where id = ?";

    private DataSource dataSource;

    public ContactDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public int add(ContactModel contact) throws TransactionException {
        log.info("Insert a new contact ({} {}) into DB", contact.getFirstName(), contact.getLastName());

        PreparedStatement stmt = null;
        ResultSet keyRs = null;
        try {
            Connection connection = dataSource.getConnection();
            stmt = connection.prepareStatement(INSERT_SQL);
            stmt.setString(1, contact.getFirstName());
            stmt.setString(2, contact.getLastName());
            stmt.setString(3, contact.getMiddleName());
            stmt.setDate(4, contact.getBirthday());
            stmt.setString(5, contact.getSex());
            stmt.setString(6, contact.getNationality());
            stmt.setString(7, contact.getMaritalStatus());
            stmt.setString(8, contact.getSite());
            stmt.setString(9, contact.getEmail());
            stmt.setString(10, contact.getCurrentJob());
            if (!contact.getAddress().isEmpty()) {
                stmt.setInt(11, contact.getAddress().getId());
            } else {
                stmt.setNull(11, Types.INTEGER);
            }
            stmt.setString(12, contact.getAvatar());

            if (stmt.executeUpdate() == 0) {
                throw new SQLException("Failed to insert a contact, no rows affected");
            }
            keyRs = stmt.getGeneratedKeys();
            if (keyRs.next()) {
                return keyRs.getInt(1);
            } else {
                throw new SQLException("Failed to insert a contact, no ID obtained");
            }
        } catch (SQLException e) {
            throw new TransactionException("Insert Contact exception", e);
        } finally {
            closeQuietly(keyRs, stmt);
        }
    }

    @Override
    public int update(ContactModel contact) throws TransactionException {
        log.info("Update the contact with id: {}", contact.getId());

        PreparedStatement stmt = null;
        try {
            Connection connection = dataSource.getConnection();
            stmt = connection.prepareStatement(UPDATE_SQL);
            stmt.setString(1, contact.getFirstName());
            stmt.setString(2, contact.getLastName());
            stmt.setString(3, contact.getMiddleName());
            stmt.setDate(4, contact.getBirthday());
            stmt.setString(5, contact.getSex());
            stmt.setString(6, contact.getNationality());
            stmt.setString(7, contact.getMaritalStatus());
            stmt.setString(8, contact.getSite());
            stmt.setString(9, contact.getEmail());
            stmt.setString(10, contact.getCurrentJob());
            if (contact.getAddress().getId() != -1) {
                stmt.setInt(11, contact.getAddress().getId());
            } else {
                stmt.setNull(11, Types.INTEGER);
            }
            stmt.setString(12, contact.getAvatar());
            stmt.setInt(13, contact.getId());
            return stmt.executeUpdate();
        } catch (SQLException e) {
            throw new TransactionException("Update Contact exception", e);
        } finally {
            closeQuietly(null, stmt);
        }
    }

    @Override
    public ContactModel get(int id) throws TransactionException {
        log.info("Get a contact by id: {}", id);

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            Connection connection = dataSource.getConnection();
            stmt = connection.prepareStatement(SELECT_SQL);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                ContactModel contact = new ContactModel(
                        rs.getString("f_name"),
                        rs.getString("l_name"));
                contact.setId(rs.getInt("contact.id"));
                contact.setMiddleName(rs.getString("m_name"));
                contact.setBirthday(rs.getDate("birthday"));
                contact.setSex(rs.getString("sex"));
                contact.setNationality(rs.getString("nationality"));
                contact.setMaritalStatus(rs.getString("marital_status"));
                contact.setSite(rs.getString("site"));
                contact.setEmail(rs.getString("email"));
                contact.setCurrentJob(rs.getString("current_job"));
                contact.setAvatar(rs.getString("avatar"));

                AddressModel address = new AddressModel();
                address.setId(rs.getInt("address.id"));
                address.setCountry(rs.getString("country"));
                address.setCity(rs.getString("city"));
                address.setDetailAddress(rs.getString("detail_address"));
                address.setZip(rs.getInt("zip"));
                contact.setAddress(address);
                return contact;
            } else{
                throw new TransactionException("No such contact for id = '" + id + "'");
            }
        } catch (SQLException e) {
            throw new TransactionException("Select Contact exception", e);
        } finally {
            closeQuietly(rs, stmt);
        }
    }

    @Override
    public List<ContactModel> getContactsByPage(int page, int amount) throws TransactionException {
        log.info("Get a contacts list by page: {}", page);

        List<ContactModel> contacts = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            Connection connection = dataSource.getConnection();
            stmt = connection.prepareStatement(SELECT_ALL_SQL);
            stmt.setInt(1, amount + 1);
            stmt.setInt(2, (page - 1) * amount);

            rs = stmt.executeQuery();
            fillContactsList(rs, contacts);
        } catch (SQLException e) {
            throw new TransactionException("Select all Contacts exception", e);
        } finally {
            closeQuietly(rs, stmt);
        }
        return contacts;
    }

    @Override
    public List<ContactModel> getContactsBySearch(int page, int amount, SearchCriteria criteria)
            throws TransactionException {
        log.info("Get a contacts list by search: {}", criteria.toString());

        List<ContactModel> contacts = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        int index = 1;
        try {
            Connection connection = dataSource.getConnection();
            String sql = getSearchSql(criteria);
            log.debug("Search SQL: '{}'", sql);
            stmt = connection.prepareStatement(sql);
            if (criteria.getAddress().getZip() != 0) {
                stmt.setString(index++, "%" + String.valueOf(criteria.getAddress().getZip()) + "%");
            }
            if (criteria.getAddress().getDetailAddress() != null) {
                stmt.setString(index++, "%" + criteria.getAddress().getDetailAddress() + "%");
            }
            if (criteria.getAddress().getCity() != null) {
                stmt.setString(index++, "%" + criteria.getAddress().getCity() + "%");
            }
            if (criteria.getAddress().getCountry() != null) {
                stmt.setString(index++, "%" + criteria.getAddress().getCountry() + "%");
            }
            if (criteria.getBirthdayFrom() != null || criteria.getBirthdayTo() != null) {
                stmt.setDate(index++, criteria.getBirthdayFrom());
                stmt.setDate(index++, criteria.getBirthdayTo());
            }
            if (criteria.getMaritalStatus() != null) {
                stmt.setString(index++, criteria.getMaritalStatus());
            }
            if (criteria.getNationality() != null) {
                stmt.setString(index++, "%" + criteria.getNationality() + "%");
            }
            if (criteria.getSex() != null) {
                stmt.setString(index++, criteria.getSex());
            }
            if (criteria.getMiddleName() != null) {
                stmt.setString(index++, "%" + criteria.getMiddleName() + "%");
            }
            if (criteria.getLastName() != null) {
                stmt.setString(index++, "%" + criteria.getLastName() + "%");
            }
            if (criteria.getFirstName() != null) {
                stmt.setString(index++, "%" + criteria.getFirstName() + "%");
            }
            stmt.setInt(index++, amount + 1);
            stmt.setInt(index, (page - 1) * amount);

            log.debug("PreparedStatement: '{}'", stmt);

            rs = stmt.executeQuery();
            fillContactsList(rs, contacts);
        } catch (SQLException e) {
            throw new TransactionException("Select all Contacts exception", e);
        } finally {
            closeQuietly(rs, stmt);
        }
        return contacts;
    }

    @Override
    public List<ContactModel> getContactsByBirthday(Date date) throws TransactionException {
        log.info("Get a contacts list by birthday: {}",
                date.toLocalDate().format(DateTimeFormatter.ofPattern("MM/dd")));

        List<ContactModel> contacts = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            Connection connection = dataSource.getConnection();
            stmt = connection.prepareStatement(SELECT_ALL_BIRTHDAY_SQL);
            stmt.setDate(1, date);

            rs = stmt.executeQuery();
            while (rs.next()) {
                ContactModel contact = new ContactModel(
                        rs.getString("f_name"),
                        rs.getString("l_name"));
                contact.setId(rs.getInt("id"));
                contact.setBirthday(rs.getDate("birthday"));
                contact.setEmail(rs.getString("email"));
                contacts.add(contact);
            }
        } catch (SQLException e) {
            throw new TransactionException("Select Contacts by birthday exception", e);
        } finally {
            closeQuietly(rs, stmt);
        }
        return contacts;
    }

    private void fillContactsList(ResultSet rs, List<ContactModel> contacts) throws SQLException {
        while (rs.next()) {
            ContactModel contact = new ContactModel(
                    rs.getString("f_name"),
                    rs.getString("l_name"));
            contact.setId(rs.getInt("id"));
            contact.setMiddleName(rs.getString("m_name"));
            contact.setBirthday(rs.getDate("birthday"));
            contact.setCurrentJob(rs.getString("current_job"));
            contact.setEmail(rs.getString("email"));

            AddressModel address = new AddressModel();
            address.setCountry(rs.getString("country"));
            address.setCity(rs.getString("city"));
            address.setDetailAddress(rs.getString("detail_address"));
            address.setZip(rs.getInt("zip"));
            contact.setAddress(address);
            contacts.add(contact);
        }
    }

    private String getSearchSql(SearchCriteria criteria) {
        StringBuilder generatedSql = new StringBuilder(SELECT_ALL_SQL);
        int offset = generatedSql.indexOf("WHERE") + "WHERE".length();
        if (criteria.getFirstName() != null) {
            generatedSql.insert(offset, " contact.f_name LIKE ? AND");
        }
        if (criteria.getLastName() != null) {
            generatedSql.insert(offset, " contact.l_name LIKE ? AND");
        }
        if (criteria.getMiddleName() != null) {
            generatedSql.insert(offset, " contact.m_name LIKE ? AND");
        }
        if (criteria.getSex() != null) {
            generatedSql.insert(offset, " contact.sex = ? AND");
        }
        if (criteria.getNationality() != null) {
            generatedSql.insert(offset, " contact.nationality LIKE ? AND");
        }
        if (criteria.getMaritalStatus() != null) {
            generatedSql.insert(offset, " contact.marital_status = ? AND");
        }
        if (criteria.getBirthdayFrom() != null || criteria.getBirthdayTo() != null) {
            generatedSql.insert(offset, " contact.birthday BETWEEN ifnull(?, '1900-01-01') AND ifnull(?, now()) AND");
        }
        if (criteria.getAddress().getCountry() != null) {
            generatedSql.insert(offset, " address.country LIKE ? AND");
        }
        if (criteria.getAddress().getCity() != null) {
            generatedSql.insert(offset, " address.city LIKE ? AND");
        }
        if (criteria.getAddress().getDetailAddress() != null) {
            generatedSql.insert(offset, " address.detail_address LIKE ? AND");
        }
        if (criteria.getAddress().getZip() != 0) {
            generatedSql.insert(offset, " CAST(address.zip AS CHAR) LIKE ? AND");
        }
        return generatedSql.toString();
    }

    @Override
    public int delete(int id) throws TransactionException {
        log.info("Mark the contact with id: {} as deleted", id);

        PreparedStatement stmt = null;
        try {
            Connection connection = dataSource.getConnection();
            stmt = connection.prepareStatement(DELETE_SQL);
            stmt.setTimestamp(1, Timestamp.from(Instant.now()));
            stmt.setInt(2, id);
            return stmt.executeUpdate();
        } catch (SQLException e) {
            throw new TransactionException("Delete Contact exception", e);
        } finally {
            closeQuietly(null, stmt);
        }
    }
}