package com.itechart.contacts.db.dao;

import com.itechart.contacts.db.model.ContactModel;
import com.itechart.contacts.db.model.SearchCriteria;
import com.itechart.contacts.exception.TransactionException;

import java.sql.Date;
import java.util.List;

public interface ContactDao {

    /**
     * @param contact
     * @return The generated id of the added contact within DB.
     * @throws TransactionException
     */
    int add(ContactModel contact) throws TransactionException;

    int update(ContactModel contact) throws TransactionException;

    ContactModel get(int id) throws TransactionException;

    List<ContactModel> getContactsByPage(int page, int amount) throws TransactionException;

    List<ContactModel> getContactsBySearch(int page, int amount, SearchCriteria criteria) throws TransactionException;

    List<ContactModel> getContactsByBirthday(Date birthday) throws TransactionException;

    int delete(int id) throws TransactionException;
}