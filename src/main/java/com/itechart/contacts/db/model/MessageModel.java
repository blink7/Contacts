package com.itechart.contacts.db.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MessageModel {
    @SerializedName("recipients")
    private List<Integer> recipients;
    @SerializedName("subject")
    private String subject;
    @SerializedName("template")
    private String template;
    @SerializedName("text")
    private String text;

    public List<Integer> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<Integer> recipients) {
        this.recipients = recipients;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}