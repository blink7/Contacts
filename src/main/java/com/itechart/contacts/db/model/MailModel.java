package com.itechart.contacts.db.model;

import java.sql.Date;

public class MailModel {
    private int contactId;
    private String subject;
    private String text;
    private Date sendDate;

    public MailModel(int contactId, String subject, String text) {
        this.contactId = contactId;
        this.subject = subject;
        this.text = text;
    }

    public int getContactId() {
        return contactId;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }
}