package com.itechart.contacts.listener;

import com.itechart.contacts.mail.MailBirthdayJob;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

@WebListener
public class BirthdayListener implements ServletContextListener {

    private static final Logger log = LoggerFactory.getLogger(BirthdayListener.class);

    private Scheduler scheduler;

    public BirthdayListener() {
        SchedulerFactory schedulerFact = new StdSchedulerFactory();
        try {
            scheduler = schedulerFact.getScheduler();
        } catch (SchedulerException e) {
            log.error("Failed to initialize the birthday scheduler", e);
        }
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        if (scheduler != null) {
            try {
                scheduler.start();

                JobDetail job = newJob(MailBirthdayJob.class)
                        .withIdentity("birthdayJob", "group1")
                        .build();

                Trigger trigger = newTrigger()
                        .withIdentity("dailyTrigger", "group1")
                        .startNow()
                        .withSchedule(simpleSchedule()
                                .withIntervalInHours(24)
                                .repeatForever())
                        .build();

                scheduler.scheduleJob(job, trigger);
            } catch (SchedulerException e) {
                log.error("Cannot schedule the birthday job", e);
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        if (scheduler != null) {
            try {
                scheduler.shutdown();
            } catch (SchedulerException ignore) {
                /*NOP*/
            }
        }
    }
}