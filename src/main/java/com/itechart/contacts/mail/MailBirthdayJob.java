package com.itechart.contacts.mail;

import com.itechart.contacts.db.dao.ContactDao;
import com.itechart.contacts.db.dao.implementation.ContactDaoImpl;
import com.itechart.contacts.db.model.ContactModel;
import com.itechart.contacts.db.transaction.TransactionManager;
import com.itechart.contacts.db.transaction.TransactionManagerImpl;
import com.itechart.contacts.exception.TransactionException;
import com.itechart.contacts.util.TemplateHelper;
import com.itechart.contacts.util.Utils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

public class MailBirthdayJob implements Job {

    private static final Logger log = LoggerFactory.getLogger(MailBirthdayJob.class);

    private TransactionManager txManger;
    private ContactDao contactDao;

    public MailBirthdayJob() {
        txManger = new TransactionManagerImpl();
        contactDao = new ContactDaoImpl((TransactionManagerImpl) txManger);
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        final Date now = Date.valueOf(LocalDate.now());
        try {
            List<ContactModel> contacts = txManger.executeTransaction(() -> contactDao.getContactsByBirthday(now));
            if (!contacts.isEmpty()) {
                log.info("Found {} contacts with today's birthdays", contacts.size());

                String subject = Utils.getMessage("template.birthdays_admin_alert", null);
                String text = TemplateHelper.getAdminMsg("birthdays", contacts);

                ResourceBundle resource = ResourceBundle.getBundle("mail", Locale.getDefault());
                MailHelper.sendMail(resource.getString("mail.admin"), subject, text);
            } else {
                log.info("No contacts found with today's birthdays");
            }
        } catch (TransactionException e) {
            log.error(e.getMessage(), e);
        } catch (MessagingException e) {
            log.error("Failed to send messages", e);
        }
    }
}