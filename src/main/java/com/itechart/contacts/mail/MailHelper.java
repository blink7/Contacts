package com.itechart.contacts.mail;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

public class MailHelper {

    public static void sendMail(final String recipients, final String subject,
                                final String text) throws MessagingException {

        ResourceBundle resource = ResourceBundle.getBundle("mail", Locale.getDefault());
        Properties properties = new Properties();
        Enumeration<String> keys = resource.getKeys();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            properties.put(key, resource.getString(key));
        }

        Session session = Session.getInstance(properties,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(
                                properties.getProperty("mail.user"),
                                properties.getProperty("mail.password"));
                    }
                });

        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(properties.getProperty("mail.from")));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipients));
        setSubject(message, subject);
        setText(message, text);

        Transport.send(message);
    }

    private static void setSubject(MimeMessage message, String subject) throws MessagingException {
        if (subject != null) {
            message.setSubject(subject, "UTF-8");
        }
    }

    private static void setText(MimeMessage message, String text) throws MessagingException {
        if (text != null) {
            message.setText(text, "UTF-8");
        }
    }
}