package com.itechart.contacts.controller;

import com.itechart.contacts.db.dao.*;
import com.itechart.contacts.db.dao.implementation.*;
import com.itechart.contacts.db.transaction.TransactionManager;
import com.itechart.contacts.db.transaction.TransactionManagerImpl;
import com.itechart.contacts.service.ContactsService;
import com.itechart.contacts.service.EmailService;
import com.itechart.contacts.storage.FileStorage;
import com.itechart.contacts.storage.LocalFileStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ContactsController",
        urlPatterns = {"/contacts", "/contacts/create", "/contacts/edit",
        "/contacts/delete", "/contacts/save", "/contacts/photo", "/contacts/search", "/contacts/send"})
public class ContactsController extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(ContactsController.class);

    private ContactsService service;
    private EmailService emailService;

    @Override
    public void init() throws ServletException {
        super.init();

        TransactionManager txManger = new TransactionManagerImpl();
        ContactDao contactDao = new ContactDaoImpl((TransactionManagerImpl) txManger);;
        AddressDao addressDao = new AddressDaoImpl((TransactionManagerImpl) txManger);
        AttachmentDao attachmentDao = new AttachmentDaoImpl((TransactionManagerImpl) txManger);
        PhoneDao phoneDao = new PhoneDaoImpl((TransactionManagerImpl) txManger);
        MailDao mailDao = new MailDaoImpl((TransactionManagerImpl) txManger);
        FileStorage storage = new LocalFileStorage();

        service = new ContactsService(getServletContext(), txManger, contactDao,
                addressDao, attachmentDao, phoneDao, storage);
        emailService = new EmailService(txManger, contactDao, mailDao);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] pathParts = req.getRequestURI().split("/");
        if (pathParts.length == 2 && pathParts[1].equalsIgnoreCase("contacts")) {
            service.loadContactsList(req, resp);
        } else if (pathParts.length == 3) {
            if (pathParts[2].equalsIgnoreCase("create")) {
                service.createContact(req, resp);
            } else if (pathParts[2].equalsIgnoreCase("delete")) {
                service.deleteContact(req, resp);
            } else if (pathParts[2].equalsIgnoreCase("edit")) {
                service.loadContact(req, resp);
            } else if (pathParts[2].equalsIgnoreCase("save")) {
                service.saveContact(req, resp);
            } else if (pathParts[2].equalsIgnoreCase("photo")) {
                service.loadPhoto(req, resp);
            } else if (pathParts[2].equalsIgnoreCase("search")) {
                service.searchContacts(req, resp);
            } else if (pathParts[2].equalsIgnoreCase("send")) {
                emailService.sendMail(req, resp);
            } else {
                resp.sendError(404, "Page not found!");
            }
        } else {
            resp.sendError(404, "Page not found!");
        }
    }
}