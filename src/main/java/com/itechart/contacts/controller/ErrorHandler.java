package com.itechart.contacts.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.itechart.contacts.util.Utils.*;

@WebServlet("/error")
public class ErrorHandler extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(ErrorHandler.class);

    private static final String ERROR = "/WEB-INF/jsp/error.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processError(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processError(req, resp);
    }

    private void processError(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer statusCode = (Integer) req.getAttribute("javax.servlet.error.status_code");
        Throwable throwable = (Throwable) req.getAttribute("javax.servlet.error.exception");

        String message;
        if (statusCode == 404) {
            message = getMessage("message.not_found", null);
        } else if (statusCode == 400) {
            message = (String) req.getAttribute("javax.servlet.error.message");
        } else {
            message = getMessage("field.error_msg", null);
            if (throwable != null) {
                log.error(throwable.getMessage(), throwable);
            }
        }
        req.setAttribute("status_code", statusCode);
        req.setAttribute("error_msg", message);
        req.getRequestDispatcher(ERROR).forward(req, resp);
    }
}