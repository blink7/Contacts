package com.itechart.contacts.service;

import com.google.gson.Gson;
import com.itechart.contacts.db.dao.ContactDao;
import com.itechart.contacts.db.dao.MailDao;
import com.itechart.contacts.db.model.ContactModel;
import com.itechart.contacts.db.model.MailModel;
import com.itechart.contacts.db.model.MessageModel;
import com.itechart.contacts.db.transaction.TransactionManager;
import com.itechart.contacts.exception.TransactionException;
import com.itechart.contacts.mail.MailHelper;
import com.itechart.contacts.util.TemplateHelper;
import com.itechart.contacts.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.itechart.contacts.service.ContactsService.ACTION_MESSAGE;
import static com.itechart.contacts.util.Utils.getMessage;

public class EmailService {

    private static final Logger log = LoggerFactory.getLogger(EmailService.class);

    private TransactionManager txManger;
    private ContactDao contactDao;
    private MailDao mailDao;

    public EmailService(TransactionManager txManger, ContactDao contactDao, MailDao mailDao) {
        this.txManger = txManger;
        this.contactDao = contactDao;
        this.mailDao = mailDao;
    }

    public void sendMail(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String contentJson = req.getParameter("content");
        log.info("Send email from content: '{}'", contentJson);

        Gson gson = Utils.buildGsonInstance();
        final MessageModel messageModel = gson.fromJson(contentJson, MessageModel.class);
        if (messageModel == null || messageModel.getRecipients().isEmpty()) {
            resp.sendRedirect("/contacts");
            return;
        }

        try {
            List<ContactModel> contacts = txManger.executeTransaction(() -> {
                List<ContactModel> contactModels = new ArrayList<>();
                for (int id : messageModel.getRecipients()) {
                    contactModels.add(contactDao.get(id));
                }
                return contactModels;
            });
            if (contacts.isEmpty()) {
                throw new TransactionException("No contacts to send a message.");
            }

            for (ContactModel contact : contacts) {
                if (contact.getEmail() == null) {
                    continue;
                }

                String text;
                if (messageModel.getTemplate() != null) {
                    text = TemplateHelper.getRenderedMsg(messageModel.getTemplate(), contact);
                } else {
                    text = messageModel.getText();
                }

                log.debug(">> Mail: '{}', '{}', '{}'", contact.getEmail(), messageModel.getSubject(), text);

                MailHelper.sendMail(contact.getEmail(), messageModel.getSubject(), text);

                final MailModel mailModel = new MailModel(contact.getId(), messageModel.getSubject(), text);
                txManger.executeTransaction(() -> mailDao.archiveMail(mailModel));
            }
            req.getSession().setAttribute(ACTION_MESSAGE, getMessage("message.send_ok", null));
            resp.sendRedirect("/contacts");
        } catch (TransactionException e) {
            log.error("Cannot get contacts for IDs: '{}'", messageModel.getRecipients()
                    .stream().map(String::valueOf).collect(Collectors.joining(",")));
            log.error(e.getMessage(), e);
            resp.sendError(400, getMessage("message.mail", null));
        } catch (MessagingException e) {
            log.error("Cannot send an email", e);
            resp.sendError(500, "Cannot send an email");
        }
    }
}