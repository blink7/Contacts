package com.itechart.contacts.service;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.itechart.contacts.db.dao.AddressDao;
import com.itechart.contacts.db.dao.AttachmentDao;
import com.itechart.contacts.db.dao.ContactDao;
import com.itechart.contacts.db.dao.PhoneDao;
import com.itechart.contacts.db.model.*;
import com.itechart.contacts.db.transaction.TransactionManager;
import com.itechart.contacts.exception.StorageException;
import com.itechart.contacts.exception.TransactionException;
import com.itechart.contacts.storage.FileStorage;
import com.itechart.contacts.storage.LocalFileStorage;
import com.itechart.contacts.util.TemplateHelper;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.itechart.contacts.util.Utils.*;

public class ContactsService {

    private static final Logger log = LoggerFactory.getLogger(ContactsService.class);

    public static final int CONTACTS_AMOUNT_PER_PAGE = 10;
    public static final String CURRENT_PAGE_KEY = "currentPage";
    public static final String CURRENT_SEARCH_PAGE_KEY = "currentSearchPage";

    public static final String LIST = "/WEB-INF/jsp/list.jsp";
    public static final String EDIT = "/WEB-INF/jsp/edit.jsp";
    private static final String PHOTO_PLACEHOLDER = "/resources/images/avatar-placeholder.png";

    private static final String PAGE_KEY = "page";
    private static final String IS_LAST_PAGE_KEY = "lastPage";
    private static final String TEMPLATES_KEY = "templates";

    private static final String EMPTY_PHONES_KEY = "emptyPhoneTable";
    private static final String EMPTY_ATTACHMENTS_KEY = "emptyAttachmentTable";
    private static final String CURRENT_CONTACT_KEY = "currentContact";

    private static final String SEARCH_CRITERIA_KEY = "searchCriteria";
    private static final String SEARCH_RESULT_KEY = "searchResult";

    public static final String ACTION_MESSAGE = "actionMessage";

    private ServletContext context;

    private TransactionManager txManger;
    private ContactDao contactDao;
    private AddressDao addressDao;
    private AttachmentDao attachmentDao;
    private PhoneDao phoneDao;

    private FileStorage storage;

    public ContactsService(ServletContext context, TransactionManager txManger, ContactDao contactDao,
                           AddressDao addressDao, AttachmentDao attachmentDao, PhoneDao phoneDao, FileStorage storage) {
        this.context = context;
        this.txManger = txManger;
        this.contactDao = contactDao;
        this.addressDao = addressDao;
        this.attachmentDao = attachmentDao;
        this.phoneDao = phoneDao;
        this.storage = storage;
    }

    public void loadContactsList(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String sPage = req.getParameter("page");
        int currentPage = (int) req.getSession().getAttribute(CURRENT_PAGE_KEY);

        log.info("Get contacts for page: '{}'...", sPage);

        try {
            int loadablePage;
            if (sPage == null) {
                loadablePage = currentPage;
            } else {
                loadablePage = Integer.parseInt(sPage);
            }
            currentPage = loadablePage;

            List<ContactModel> contacts = txManger.executeTransaction(
                    () -> contactDao.getContactsByPage(loadablePage, CONTACTS_AMOUNT_PER_PAGE));
            log.info("Loaded '{}' contacts", contacts.size());
            if (contacts.isEmpty() && currentPage != 1) {
                currentPage = 1;
                resp.sendRedirect("/contacts?page=" + currentPage);
                return;
            } else if (contacts.size() <= CONTACTS_AMOUNT_PER_PAGE) {
                req.setAttribute(IS_LAST_PAGE_KEY, true);
                req.setAttribute("contacts", contacts);
            } else {
                req.setAttribute("contacts", contacts.subList(0, CONTACTS_AMOUNT_PER_PAGE));
            }
            req.setAttribute(TEMPLATES_KEY, TemplateHelper.getAllMsgs());
            req.setAttribute(PAGE_KEY, loadablePage);

            getResult(req);
            req.getSession().setAttribute(CURRENT_PAGE_KEY, currentPage);

            req.getRequestDispatcher(LIST).forward(req, resp);
            return;
        } catch (TransactionException e) {
            log.error("Failed getting a list of contacts for page: '{}'", sPage);
            log.error(e.getMessage(), e);
        }
        resp.sendError(400, getMessage("message.contact_list_error", null));
    }

    private void getResult(HttpServletRequest req) {
        String result = (String) req.getSession().getAttribute(ACTION_MESSAGE);
        if (result != null) {
            req.setAttribute(ACTION_MESSAGE, result);
        }
        req.getSession().setAttribute(ACTION_MESSAGE, null);
    }

    private void sendResult(HttpServletRequest req, String result) {
        req.getSession().setAttribute(ACTION_MESSAGE, result);
    }

    public void createContact(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("Create new contact..");

        req.getSession().setAttribute(CURRENT_CONTACT_KEY, null);
        req.setAttribute(EMPTY_PHONES_KEY, true);
        req.setAttribute(EMPTY_ATTACHMENTS_KEY, true);
        req.setAttribute("forEdit", false);
        req.getRequestDispatcher(EDIT).forward(req, resp);
    }

    public void loadContact(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().setAttribute(CURRENT_CONTACT_KEY, null);
        String sId = req.getParameter("id");

        log.info("Get a contact by id: '{}'...", sId);

        try {
            int id = Integer.parseInt(sId);
            ContactModel contact = txManger.executeTransaction(() -> {
                ContactModel contactModel = contactDao.get(id);
                contactModel.setPhones(phoneDao.get(id));
                contactModel.setAttachments(attachmentDao.get(id));
                return contactModel;
            });

            req.getSession().setAttribute(CURRENT_CONTACT_KEY, contact);
            if (contact.getPhones().isEmpty()) {
                req.setAttribute(EMPTY_PHONES_KEY, true);
            }
            if (contact.getAttachments().isEmpty()) {
                req.setAttribute(EMPTY_ATTACHMENTS_KEY, true);
            }

            req.setAttribute("forEdit", true);
            req.setAttribute("contact", contact);
            req.getRequestDispatcher(EDIT).forward(req, resp);
            return;
        } catch (TransactionException e) {
            log.error("Failed getting a contact by id: '{}'", sId);
            log.error(e.getMessage(), e);
        }
        resp.sendError(400, getMessage("message.contact_error", null));
    }

    public void saveContact(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DiskFileItemFactory factory = new DiskFileItemFactory();
        File repository = (File) context.getAttribute("javax.servlet.context.tempdir");
        factory.setRepository(repository);

        ServletFileUpload upload = new ServletFileUpload(factory);
        try {
            ContactModel savableContact = null;
            List<FileItem> items = upload.parseRequest(req);
            Map<String, FileItem> uploadedFiles = new HashMap<>();
            for (FileItem item : items) {
                if (item.isFormField() && item.getFieldName().equalsIgnoreCase("contact_json")) {
                    String contactJson = item.getString("UTF-8");
                    log.debug("Contact JSON: {}", contactJson);
                    Gson gson = buildGsonInstance();
                    savableContact = gson.fromJson(contactJson, ContactModel.class);
                    log.debug("Savable contact: {}", savableContact.toString());
                } else {
                    log.debug("Uploaded file: {}", item.getFieldName());
                    uploadedFiles.put(item.getFieldName(), item);
                }
            }

            if (savableContact != null) {
                if (savableContact.getFirstName() == null || savableContact.getFirstName().isEmpty()
                        || savableContact.getLastName() == null || savableContact.getLastName().isEmpty()) {
                    throw new IllegalArgumentException("First Name or/and Last Name are Empty!");
                }
                if (!validateEmail(savableContact.getEmail())) {
                    throw new IllegalArgumentException("Invalid email format");
                }

                ContactModel currentContact = (ContactModel) req.getSession().getAttribute(CURRENT_CONTACT_KEY);
                if (currentContact != null && currentContact.getId() != savableContact.getId()) {
                    throw new IllegalArgumentException("The id of a savable contact is not correct!");
                }

                if (currentContact != null) {
                    performUpdateContact(savableContact, currentContact, uploadedFiles);
                    sendResult(req, getMessage("message.save_ok", null));
                } else {
                    performCreateContact(savableContact, uploadedFiles);
                    sendResult(req, getMessage("message.create_ok", null));
                }
            }

            resp.sendRedirect("/contacts");
            return;
        } catch (FileUploadException e) {
            log.error("Failed to parse the request data", e);
        } catch (JsonParseException | NumberFormatException e) {
            log.error("Failed to retrieve the JSON Contact data", e);
        } catch (IllegalArgumentException e) {
            log.error(e.getMessage(), e);
        } catch (TransactionException e) {
            log.error("Cannot save the contact", e);
        }
        resp.sendError(400, getMessage("message.save_error", null));
    }

    private void performUpdateContact(final ContactModel contact, final ContactModel currentContact,
                                      Map<String, FileItem> uploadedFiles) throws TransactionException {
        log.info("Process update the contact with id: '{}'", contact.getId());

        txManger.executeTransaction(() -> {
            try {
                AddressModel address = contact.getAddress();
                if (!address.isEmpty() && currentContact.getAddress().isEmpty()) {
                    int addressId = addressDao.add(address);
                    address.setId(addressId);
                } else if (!address.isEmpty() && !currentContact.getAddress().isEmpty()) {
                    address.setId(currentContact.getAddress().getId());
                    addressDao.update(address);
                } else if (address.isEmpty() && !currentContact.getAddress().isEmpty()) {
                    addressDao.delete(currentContact.getAddress().getId());
                    address.setId(-1);
                }

                int contactId = currentContact.getId();

                FileItem photo = uploadedFiles.get("photo");
                if (photo != null && validatePhoto(photo.getName())) {
                    String photoUuid = storage.saveFile(contactId, photo.getInputStream());
                    contact.setAvatar(photoUuid);
                } else {
                    contact.setAvatar(currentContact.getAvatar());
                }

                contactDao.update(contact);

                List<PhoneModel> phones = contact.getPhones();
                for (PhoneModel phone : phones) {
                    if (validatePhone(phone)) {
                        String mark = phone.getMark();
                        if ("is-new".equals(mark)) {
                            phone.setContactId(contactId);
                            phoneDao.add(phone);
                        } else if ("is-edited".equals(mark)) {
                            phoneDao.update(phone);
                        } else if ("is-deleted".equals(mark)) {
                            phoneDao.delete(phone.getId());
                        }
                    }
                }

                List<AttachmentModel> attachments = contact.getAttachments();
                for (AttachmentModel attachment : attachments) {
                    String mark = attachment.getMark();
                    if ("is-new".equals(mark)) {
                        String attachmentId = "newAttachment" + attachment.getId();
                        FileItem item = uploadedFiles.get(attachmentId);
                        if (item != null) {
                            String uuid = storage.saveFile(contactId, item.getInputStream());
                            attachment.setFileUuid(uuid);
                            attachment.setFileName(item.getName());
                            attachment.setContactId(contactId);
                            attachmentDao.add(attachment);
                        }
                    } else if ("is-edited".equals(mark)) {
                        FileItem item = uploadedFiles.get("attachment" + attachment.getId());
                        Optional<AttachmentModel> optionalAttachment = currentContact.getAttachments().stream()
                                .filter(attachmentModel -> attachmentModel.getId() == attachment.getId())
                                .findFirst();
                        if (item != null && optionalAttachment.isPresent()) {
                            String currentUuid = optionalAttachment.get().getFileUuid();
                            storage.deleteFile(contactId, currentUuid);
                            String newUuid = storage.saveFile(contactId, item.getInputStream());
                            attachment.setContactId(contactId);
                            attachment.setFileUuid(newUuid);
                            attachment.setFileName(item.getName());
                            attachment.setUploadDate(Timestamp.from(Instant.now()));
                            attachmentDao.update(attachment);
                        } else if (optionalAttachment.isPresent()) {
                            optionalAttachment.get().setComment(attachment.getComment());
                            attachmentDao.update(optionalAttachment.get());
                        }
                    } else if ("is-deleted".equals(mark)) {
                        Optional<AttachmentModel> optionalAttachment = currentContact.getAttachments().stream()
                                .filter(attachmentModel -> attachmentModel.getId() == attachment.getId())
                                .findFirst();
                        if (optionalAttachment.isPresent()) {
                            storage.deleteFile(contactId, optionalAttachment.get().getFileUuid());
                            attachmentDao.delete(optionalAttachment.get().getId());
                        }
                    }
                }
                return true;
            } catch (StorageException e) {
                throw new TransactionException("Failed to update an attachment", e);
            } catch (IOException e) {
                throw new TransactionException("Failed to retrieve an attachment from an input field", e);
            }
        });
    }

    private void performCreateContact(final ContactModel contact, Map<String, FileItem> uploadedFiles)
            throws TransactionException {
        log.info("Process create a contact");

        txManger.executeTransaction(() -> {
            try {
                AddressModel address = contact.getAddress();
                if (!address.isEmpty()) {
                    int addressId = addressDao.add(address);
                    address.setId(addressId);
                } else {
                    address.setId(-1);
                }

                int contactId = contactDao.add(contact);
                contact.setId(contactId);

                FileItem photo = uploadedFiles.get("photo");
                if (photo != null && validatePhoto(photo.getName())) {
                    String photoUuid = storage.saveFile(contactId, photo.getInputStream());
                    contact.setAvatar(photoUuid);
                }

                contactDao.update(contact);

                List<PhoneModel> phones = contact.getPhones();
                for (PhoneModel phone : phones) {
                    if (validatePhone(phone)) {
                        phone.setContactId(contactId);
                        phoneDao.add(phone);
                    }
                }

                List<AttachmentModel> attachments = contact.getAttachments();
                for (AttachmentModel attachment : attachments) {
                    String attachmentId = "newAttachment" + attachment.getId();
                    FileItem item = uploadedFiles.get(attachmentId);
                    if (item != null) {
                        String uuid = storage.saveFile(contactId, item.getInputStream());
                        attachment.setFileUuid(uuid);
                        attachment.setFileName(item.getName());
                        attachment.setContactId(contactId);
                        attachmentDao.add(attachment);
                    }
                }

                return true;
            } catch (StorageException e) {
                throw new TransactionException("Failed to save an attachment", e);
            } catch (IOException e) {
                throw new TransactionException("Failed to retrieve an attachment from an input field", e);
            }
        });
    }

    private boolean validatePhone(PhoneModel phone) {
        return String.valueOf(phone.getCountryCode()).length() == 3
                && String.valueOf(phone.getOperatorCode()).length() == 2
                && String.valueOf(phone.getPhoneNumber()).length() == 7
                && phone.getType() != null && !phone.getType().isEmpty();
    }

    private boolean validateEmail(String email) {
        if (email != null) {
            String emailRegex = "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)" +
                    "|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])" +
                    "|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
            Matcher matcher = Pattern.compile(emailRegex).matcher(email);
            return matcher.find();
        }
        return true;
    }

    private boolean validatePhoto(String fileName) {
        String extension = FilenameUtils.getExtension(fileName);
        return "jpg".equalsIgnoreCase(extension)
                || "jpeg".equalsIgnoreCase(extension)
                || "png".equalsIgnoreCase(extension);
    }

    public void deleteContact(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idsJson = req.getParameter("ids");

        log.info("Delete contacts with ids: {}", idsJson);

        Gson gson = buildGsonInstance();
        try {
            int[] ids = gson.fromJson(idsJson, int[].class);

            txManger.executeTransaction(() -> {
                try {

                    for (int id : ids) {
                        for (AttachmentModel attachment : attachmentDao.get(id)) {
                            storage.deleteFile(id, attachment.getFileUuid());
                        }
                        attachmentDao.deleteAll(id);
                        phoneDao.deleteAll(id);

                        ContactModel contact = contactDao.get(id);
                        addressDao.delete(contact.getAddress().getId());
                        String avatar;
                        if ((avatar = contact.getAvatar()) != null) {
                            storage.deleteFile(id, avatar);
                        }
                        contactDao.delete(id);
                    }
                } catch (StorageException e) {
                    log.error("Cannot delete an attachment", e);
                    throw new TransactionException(e.getMessage(), e);
                }
                return true;
            });

            sendResult(req, String.format(
                    getMessage("message.delete_ok", null),
                    ids != null ? ids.length : 0));

            resp.sendRedirect("/contacts");
            return;
        } catch (JsonParseException e) {
            log.error("Not acceptable ID of a contact", e);
        } catch (TransactionException e) {
            log.error("Failed to delete chosen contacts", e);
        }
        resp.sendError(400, getMessage("message.delete_error", null));
    }

    public void loadPhoto(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("Get a contact photo");

        resp.setContentType("image/*");
        try {
            ContactModel contact = (ContactModel) req.getSession().getAttribute(CURRENT_CONTACT_KEY);
            if (contact != null && contact.getAvatar() != null) {
                File photo = storage.getFile(contact.getId(), contact.getAvatar());
                try (InputStream in = new FileInputStream(photo); OutputStream out = resp.getOutputStream()) {
                    IOUtils.copy(in, out);
                }
            } else {
                try (InputStream in = context.getResourceAsStream(PHOTO_PLACEHOLDER);
                     OutputStream out = resp.getOutputStream()) {

                    IOUtils.copy(in, out);
                }
            }
        } catch (StorageException | IllegalArgumentException e) {
            log.error("Cannot load a contact photo", e);
        }
    }

    public void searchContacts(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("Search contacts..");

        String criteriaJson = req.getParameter("criteria");
        log.debug("Criteria JSON: {}", criteriaJson);
        String sPage = req.getParameter("page");
        int currentSearchPage = (int) req.getSession().getAttribute(CURRENT_SEARCH_PAGE_KEY);

        SearchCriteria criteria;
        if (criteriaJson != null) {
            Gson gson = buildGsonInstance();
            criteria = gson.fromJson(criteriaJson, SearchCriteria.class);
            req.getSession().setAttribute(SEARCH_CRITERIA_KEY, criteria);
        } else {
            criteria = (SearchCriteria) req.getSession().getAttribute(SEARCH_CRITERIA_KEY);
        }

        if (criteria != null) {
            try {
                int loadablePage;
                if (sPage == null) {
                    loadablePage = currentSearchPage;
                } else {
                    loadablePage = Integer.parseInt(sPage);
                }
                if (loadablePage < 1) {
                    throw new NumberFormatException("Page number is less than one!");
                }
                currentSearchPage = loadablePage;

                List<ContactModel> contacts = txManger.executeTransaction(
                        () -> contactDao.getContactsBySearch(loadablePage, CONTACTS_AMOUNT_PER_PAGE, criteria));
                if (contacts.isEmpty() && currentSearchPage != 1) {
                    currentSearchPage = 1;
                    resp.sendRedirect("/contacts/search?page=" + currentSearchPage);
                    return;
                } else if (contacts.size() <= CONTACTS_AMOUNT_PER_PAGE) {
                    req.setAttribute(IS_LAST_PAGE_KEY, true);
                    req.setAttribute("contacts", contacts);
                } else {
                    req.setAttribute("contacts", contacts.subList(0, CONTACTS_AMOUNT_PER_PAGE));
                }
                req.setAttribute(PAGE_KEY, currentSearchPage);
                req.setAttribute(SEARCH_RESULT_KEY, true);
                req.setAttribute(TEMPLATES_KEY, TemplateHelper.getAllMsgs());

                getResult(req);
                req.getSession().setAttribute(CURRENT_SEARCH_PAGE_KEY, currentSearchPage);

                req.getRequestDispatcher(LIST).forward(req, resp);
                return;
            } catch (TransactionException e) {
                log.error("Failed getting the result of search");
                log.error(e.getMessage(), e);
            } catch (NumberFormatException e) {
                log.error("The page number '{}' is not an integer", sPage);
            }
            resp.sendError(400, getMessage("message.search_error", null));
        } else {
            resp.sendRedirect("/contacts");
        }
    }
}