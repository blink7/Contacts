<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <title>
        <c:choose>
            <c:when test="${searchResult == true}">
                <fmt:message key="title.search_result"/>
            </c:when>
            <c:otherwise>
                <fmt:message key="title.list"/>
            </c:otherwise>
        </c:choose>
    </title>

    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link rel="icon" href="${pageContext.request.contextPath}/resources/images/icon.png">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.teal-indigo.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">

    <script type="text/javascript" src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/getmdl-select.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/http.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/list.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/email.js"></script>
</head>
<body>
<div class="mdl-layout__container">
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <header class="mdl-layout__header">
            <c:if test="${searchResult}">
                <button class="mdl-button mdl-js-button mdl-button--icon layout-back-btn" id="back-btn">
                    <i class="material-icons">arrow_back</i>
                </button>
                <div class="mdl-tooltip" data-mdl-for="back-btn"><fmt:message key="tooltip.return"/></div>
            </c:if>
            <div class="mdl-layout__header-row">
                <span class="mdl-layout__title">
                    <c:choose>
                        <c:when test="${searchResult == true}">
                            <fmt:message key="title.search_result"/>
                        </c:when>
                        <c:otherwise>
                            <fmt:message key="title.list"/>
                        </c:otherwise>
                    </c:choose>
                </span>
                <div class="mdl-layout-spacer"></div>
                <button id="search-button" class="mdl-button mdl-js-button mdl-button--icon">
                    <i class="material-icons">search</i>
                </button>
                <div class="mdl-tooltip" data-mdl-for="search-button"><fmt:message key="tooltip.search"/></div>
                <button id="more-button" class="mdl-button mdl-js-button mdl-button--icon">
                    <i class="material-icons">more_vert</i>
                </button>
                <div class="mdl-tooltip" data-mdl-for="more-button"><fmt:message key="tooltip.more"/></div>
                <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="more-button">
                    <li disabled="disabled" class="mdl-menu__item" name="edit-btn" id="edit-btn"><fmt:message key="btn.edit"/></li>
                    <li disabled="disabled" class="mdl-menu__item" name="delete-btn" id="delete-btn"><fmt:message key="btn.delete"/></li>
                    <li disabled="disabled" class="mdl-menu__item" name="email-btn" id="email-btn"><fmt:message key="btn.send_email"/></li>
                </ul>
            </div>
        </header>
        <main class="mdl-layout__content">
            <div class="page-content contact-list">
                <table class="mdl-data-table mdl-shadow--2dp contacts-table">
                    <thead>
                    <tr>
                        <th style="width: 2%">
                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect mdl-data-table__select" for="table-header">
                                <input type="checkbox" id="table-header" class="mdl-checkbox__input"/>
                            </label>
                        </th>
                        <th style="width: 22%" class="mdl-data-table__cell--non-numeric"><fmt:message key="col.name"/></th>
                        <th style="width: 10%"><fmt:message key="col.bd"/></th>
                        <th style="width: 36%"><fmt:message key="col.address"/></th>
                        <th style="width: 30%"><fmt:message key="col.job"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${contacts}" var="contact">
                        <tr>
                            <td>
                                <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect mdl-data-table__select" for="contact<c:out value="${contact.id}"/>">
                                    <input type="checkbox" id="contact<c:out value="${contact.id}"/>" class="mdl-checkbox__input"/>
                                </label>
                            </td>
                            <td class="mdl-data-table__cell--non-numeric"><c:out value="${contact.fullName}"/></td>
                            <td><fmt:formatDate pattern="MM/dd/yyyy" value="${contact.birthday}"/></td>
                            <td><c:out value="${contact.address.fullAddress}"/></td>
                            <td><c:out value="${contact.currentJob}"/></td>
                            <c:if test="${contact.email != null}"><td class="contacts-col-email">${contact.email}</td></c:if>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <footer class="mdl-mini-footer" style="justify-content: center;">
                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--accent float-btn" id="create-btn">
                        <i class="material-icons">add</i>
                    </button>
                    <div class="mdl-tooltip" data-mdl-for="create-btn" style="right: 20px;"><fmt:message key="tooltip.create_contact"/></div>
                    <button <c:if test="${page == 1}">disabled</c:if> class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="prev-btn">
                        <fmt:message key="btn.prev"/></button>
                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" id="page-indicator" style="background-color: #757575; color: white; min-width: 20px;" disabled>
                        ${page}
                    </button>
                    <button <c:if test="${lastPage}">disabled</c:if> class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="next-btn">
                        <fmt:message key="btn.next"/>
                    </button>
                </footer>
            </div>
        </main>
    </div>
</div>

<!-- Search Contacts modal -->
<div class="modal" id="searchContactsModal">
    <div class="modal-content mdl-card mdl-shadow--4dp" style="width: 800px;">
        <div class="modal-header">
            <h4><fmt:message key="title.modal.search"/></h4>
            <hr>
        </div>
        <div class="modal-body">
            <div class="mdl-grid row search-box">
                <div class="mdl-cell mdl-cell--6-col">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" id="fname"/>
                        <label class="mdl-textfield__label" for="fname"><fmt:message key="field.f_name"/></label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" id="mname"/>
                        <label class="mdl-textfield__label" for="mname"><fmt:message key="field.m_name"/></label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-selector getmdl-select getmdl-select__fullwidth">
                        <input class="mdl-textfield__input" type="text" id="marital_status" readonly tabIndex="-1">
                        <label class="mdl-textfield__label" for="marital_status"><fmt:message key="field.marital_status"/></label>
                        <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu" for="marital_status">
                            <li class="mdl-menu__item" data-val="empty"></li>
                            <li class="mdl-menu__item" data-val="single"><fmt:message key="field.marital_status.single"/></li>
                            <li class="mdl-menu__item" data-val="married"><fmt:message key="field.marital_status.married"/></li>
                            <li class="mdl-menu__item" data-val="divorced"><fmt:message key="field.marital_status.divorced"/></li>
                            <li class="mdl-menu__item" data-val="widowed"><fmt:message key="field.marital_status.widowed"/></li>
                        </ul>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-selector getmdl-select getmdl-select__fullwidth">
                        <input class="mdl-textfield__input" id="sex" name="sex" type="text" readonly tabIndex="-1"/>
                        <label class="mdl-textfield__label" for="sex"><fmt:message key="field.sex"/></label>
                        <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu" for="sex">
                            <li class="mdl-menu__item" data-val="empty"></li>
                            <li class="mdl-menu__item" data-val="male"><fmt:message key="field.sex.male"/></li>
                            <li class="mdl-menu__item" data-val="female"><fmt:message key="field.sex.female"/></li>
                        </ul>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" id="country">
                        <label class="mdl-textfield__label" for="country"><fmt:message key="field.country"/></label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" id="detail_address">
                        <label class="mdl-textfield__label" for="detail_address"><fmt:message key="field.detail_address"/></label>
                    </div>
                </div>

                <div class="mdl-cell mdl-cell--6-col">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" id="lname"/>
                        <label class="mdl-textfield__label" for="lname"><fmt:message key="field.l_name"/></label>
                    </div>
                    <div class="mdl-grid row">
                        <div class="mdl-cell mdl-cell--4-col" style="padding-right: 0;">
                            <div style="margin-top: 50%; margin-right: 14px;">
                                <i class="material-icons">cake</i>
                                <h6><fmt:message key="field.birthday_range"/></h6>
                            </div>
                        </div>
                        <div class="mdl-cell mdl-cell--8-col">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" id="birthday_from">
                                <label class="mdl-textfield__label" for="birthday_from"><fmt:message key="field.birthday_from"/></label>
                                <span class="mdl-textfield__error"><fmt:message key="error.date"/></span>
                            </div>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" id="birthday_to">
                                <label class="mdl-textfield__label" for="birthday_to"><fmt:message key="field.birthday_to"/></label>
                                <span class="mdl-textfield__error"><fmt:message key="error.date"/></span>
                            </div>
                        </div>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" id="nationality">
                        <label class="mdl-textfield__label" for="nationality"><fmt:message key="field.nationality"/></label>
                    </div>

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" id="city">
                        <label class="mdl-textfield__label" for="city"><fmt:message key="field.city"/></label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input number-input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="zip" onkeypress="return isNumber(event)">
                        <label class="mdl-textfield__label" for="zip"><fmt:message key="field.zip"/></label>
                        <span class="mdl-textfield__error"><fmt:message key="error.zip"/></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-btns">
            <button class="mdl-button mdl-js-button mdl-button--primary" onclick="document.querySelector('#searchContactsModal').style.display = 'none';">
                <fmt:message key="btn.cancel"/>
            </button>
            <button class="mdl-button mdl-js-button mdl-button--primary" id="apply-search-btn">
                <fmt:message key="btn.search"/>
            </button>
        </div>
    </div>
</div>

<!-- Alert -->
<div class="modal modal-dialog" id="alert">
    <div class="modal-content mdl-card mdl-shadow--4dp">
        <div class="modal-body" id="alert_msg"></div>
        <div class="footer-btns">
            <button class="mdl-button mdl-js-button mdl-button--primary" onclick="document.querySelector('#alert').style.display = 'none';">
                <fmt:message key="btn.ok"/>
            </button>
        </div>
    </div>
</div>

<!-- Dialog -->
<div class="modal modal-dialog" id="dialog">
    <div class="modal-content mdl-card mdl-shadow--4dp">
        <div class="modal-body" id="dialog_msg"><fmt:message key="message.delete_confirmation"/></div>
        <div class="footer-btns">
            <button class="mdl-button mdl-js-button mdl-button--primary" onclick="document.querySelector('#dialog').style.display = 'none';">
                <fmt:message key="btn.cancel"/>
            </button>
            <button class="mdl-button mdl-js-button mdl-button--primary" id="delete-confirm-btn">
                <fmt:message key="btn.delete"/>
            </button>
        </div>
    </div>
</div>

<!-- Email box -->
<div class="modal" id="emailModal">
    <div class="modal-content mdl-card mdl-shadow--4dp" style="width: 800px;">
        <div class="modal-header">
            <h4><fmt:message key="title.modal.email"/></h4>
            <hr>
        </div>
        <div class="modal-body">
            <div class="email-to">
                <h6 class="email-to-label"><fmt:message key="field.to"/></h6>
                <div class="email-recipients"></div>
            </div>
            <hr>
            <div>
                <div class="mdl-textfield mdl-js-textfield">
                    <input class="mdl-textfield__input" type="text" id="subject">
                    <label class="mdl-textfield__label" for="subject"><fmt:message key="field.subject"/></label>
                </div>
            </div>
            <c:if test="${not empty templates}">
                <div>
                    <fmt:message key="field.templates"/>
                    <ul class="mdl-list email-templates">
                        <c:forEach items="${templates}" var="template">
                            <li class="mdl-list__item mdl-list__item--three-line email-template">
						    <span class="mdl-list__item-primary-content">
							    <span><fmt:message key="template.${template.key}"/></span>
							    <span class="mdl-list__item-text-body">${template.value}</span>
						    </span>
                                <span class="mdl-list__item-secondary-content mdl-list__item-secondary-action">
							    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="${template.key}">
								    <input type="radio" id="${template.key}" class="mdl-radio__button" name="options"/>
							    </label>
						    </span>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
            </c:if>
            <div class="mdl-textfield mdl-js-textfield">
                <textarea class="mdl-textfield__input" type="text" rows= "3" id="email_msg"></textarea>
                <label class="mdl-textfield__label" for="email_msg"><fmt:message key="field.message"/></label>
            </div>
        </div>
        <div class="footer-btns">
            <button class="mdl-button mdl-js-button mdl-button--primary" onclick="document.querySelector('#emailModal').style.display = 'none';">
                <fmt:message key="btn.cancel"/>
            </button>
            <button class="mdl-button mdl-js-button mdl-button--primary" id="send-btn">
                <fmt:message key="btn.send"/>
            </button>
        </div>
    </div>
</div>

<div id="event-toast" class="mdl-js-snackbar mdl-snackbar">
    <div class="mdl-snackbar__text"></div>
    <button class="mdl-snackbar__action" type="button"></button>
</div>

<script type="text/javascript">
    <!-- <fmt:message key="error.bad_birthday_range" var="bad_birthday_range"/>
    <fmt:message key="error.mail_no_contacts" var="mail_no_contacts"/> //-->
    var birthdayErrorMsg = '${bad_birthday_range}';
    var mailErrorMsg = '${mail_no_contacts}';
</script>

<c:if test="${actionMessage != null}">
    <script type="text/javascript">
        (function() {
            'use strict';
            var snackbarContainer = document.querySelector('#event-toast');
            setTimeout(function() {
                'use strict';
                var data = {
                    message: '${actionMessage}'
                };
                snackbarContainer.MaterialSnackbar.showSnackbar(data);
            }, 500);
        }());
    </script>
</c:if>
</body>
</html>