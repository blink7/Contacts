<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <title><fmt:message key="title.error"/> ${status_code}</title>

    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link rel="icon" href="${pageContext.request.contextPath}/resources/images/icon.png">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.teal-indigo.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">

    <script type="text/javascript" src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/error.js"></script>
</head>
<body>
<div class="mdl-layout__container">
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <header class="mdl-layout__header">
            <button class="mdl-button mdl-js-button mdl-button--icon layout-back-btn" id="back-btn">
                <i class="material-icons">arrow_back</i>
            </button>
            <div class="mdl-tooltip" data-mdl-for="back-btn"><fmt:message key="tooltip.return"/></div>
            <div class="mdl-layout__header-row">
                <span class="mdl-layout__title"><fmt:message key="title.error"/> ${status_code}</span>
            </div>
        </header>
        <main class="mdl-layout__content">
            <div class="page-content">
                <div class="error-block">
                    <img src="${pageContext.request.contextPath}/resources/images/ic_error_outline_black_48px.svg" alt="Error image.">
                    <h2>${status_code}</h2>
                    <h5><fmt:message key="field.oops_prefix"/> ${error_msg}</h5>
                </div>
            </div>
        </main>
    </div>
</div>
</body>
</html>