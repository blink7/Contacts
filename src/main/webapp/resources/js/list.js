(function () {
    'use strict';

    var dateRegex = /^(0?[1-9]|1[0-2])\/(0?[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;

    var contextPath = '/contacts';

    var contactIdPrefix = 'contact';

    var contactMdlCheckbox;

    function whenLoaded() {
        window['checkedContactCounter'] = 0;

        contactMdlCheckbox = new MdlCheckbox(checkedContactCounter, '.contacts-table',
            '#edit-btn', '#delete-btn', '#email-btn');
        contactMdlCheckbox.init();
        contactMdlCheckbox.addHeaderCheckboxListener();

        document.querySelector('#edit-btn').addEventListener('click', editContact);
        document.querySelector('#delete-btn').addEventListener('click', deleteContacts);
        document.querySelector('#create-btn').addEventListener('click', function() {
            location.href = contextPath + '/create';
        });
        document.querySelector('#prev-btn').addEventListener('click', prevPage);
        document.querySelector('#next-btn').addEventListener('click', nextPage);

        window.addEventListener('click', closeModal);
        document.querySelector('#search-button').addEventListener('click', function() {
            document.querySelector('#searchContactsModal').style.display = 'block';
        });
        document.querySelector('#apply-search-btn').addEventListener('click', applySearch);

        if (document.querySelector('#back-btn')) {
            document.querySelector('#back-btn').addEventListener('click', function () {
                location.href = contextPath;
            })
        }

        document.querySelector('#delete-confirm-btn').addEventListener('click', sendRemoveRequest);
    }

    window.addEventListener ?
        window.addEventListener("load", whenLoaded, false) :
        window.attachEvent && window.attachEvent("onload", whenLoaded);

    function MdlCheckbox(counter, tableName, editBtnId, deleteBtnId, emailBtnId) {
        this.counter = counter;
        this.tableName = tableName;
        this.editBtnId = editBtnId;
        this.deleteBtnId = deleteBtnId;
        this.emailBtnId = emailBtnId;

        this.init = function() {
            this.table = document.querySelector(this.tableName);
            this.headerCheckbox = this.table.querySelector('thead .mdl-data-table__select input');
            this.boxes = this.table.querySelectorAll('tbody .mdl-data-table__select');
        };

        this.countCheck = function(checkbox) {
            if (checkbox.checked) {
                this.counter++;
            } else {
                this.counter > 0 ? this.counter-- : this.counter;
            }

            var deleteBtn = document.querySelector(this.deleteBtnId);
            var emailBtn = document.querySelector(this.emailBtnId);
            var editBtn = document.querySelector(this.editBtnId);
            if (this.counter === 0) {
                deleteBtn.setAttribute('disabled', 'disabled');
                emailBtn.setAttribute('disabled', 'disabled');
                editBtn.setAttribute('disabled', 'disabled');
                if (this.headerCheckbox.checked) {
                    this.headerCheckbox.parentNode.MaterialCheckbox.uncheck();
                }
            } else if (this.counter === 1) {
                editBtn.removeAttribute('disabled');
                deleteBtn.removeAttribute('disabled');
                emailBtn.removeAttribute('disabled');
            } else {
                deleteBtn.removeAttribute('disabled');
                emailBtn.removeAttribute('disabled');
                editBtn.setAttribute('disabled', 'disabled');
            }
        };

        this.addCheckboxListener = function(box) {
            var checkHandler = function(event) {
                this.countCheck(event.target);
                if (event.target.checked) {
                    event.target.parentNode.parentNode.parentNode.classList.add('is-selected');
                } else {
                    event.target.parentNode.parentNode.parentNode.classList.remove('is-selected');
                }
                if (sessionStorage.getItem(this.storageName)) {
                    this.headerCheckbox.parentNode.MaterialCheckbox.uncheck();
                }
            };
            box.addEventListener('change', checkHandler.bind(this));
        };

        this.addHeaderCheckboxListener = function() {
            var headerCheckHandler = function(event) {
                if (event.target.checked) {
                    for (var i = 0; i < this.boxes.length; i++) {
                        if (!this.boxes[i].querySelector('input.mdl-checkbox__input').checked) {
                            this.boxes[i].MaterialCheckbox.check();
                            this.boxes[i].parentNode.parentNode.classList.add('is-selected');
                            this.countCheck(event.target);
                        }
                    }
                } else {
                    for (var i = 0; i < this.boxes.length; i++) {
                        if (this.boxes[i].querySelector('input.mdl-checkbox__input').checked) {
                            this.boxes[i].MaterialCheckbox.uncheck();
                            this.boxes[i].parentNode.parentNode.classList.remove('is-selected');
                            this.countCheck(event.target);
                        }
                    }
                }
            };
            this.headerCheckbox.addEventListener('change', headerCheckHandler.bind(this));

            for (var i = 0; i < this.boxes.length; i++) {
                this.addCheckboxListener(this.boxes[i]);
                this.boxes[i].parentNode.parentNode.querySelector('td:nth-child(2)')
                    .addEventListener('click', _editContact);
            }
        }
    }

    function editContact(event) {
        if (event.target.getAttribute('disabled') === 'disabled') {
            return;
        }

        var contactRows = document.querySelectorAll('table.contacts-table tbody tr');
        var index = getSelectedIndex(contactRows);
        var editableContactItems
            = document.querySelectorAll('table.contacts-table tbody tr')[index].querySelectorAll('td');
        var contactId = editableContactItems[0].querySelector('input').getAttribute('id');
        var subId = contactId.substr(contactIdPrefix.length, contactId.length);
        sendRequest(contextPath + '/edit', {id: subId}, 'GET');
    }

    function _editContact(event) {
        var contactId = event.target.parentNode.querySelector('input').getAttribute('id');
        var subId = contactId.substr(contactIdPrefix.length, contactId.length);
        sendRequest(contextPath + '/edit', {id: subId}, 'GET');
    }

    function deleteContacts(event) {
        if (event.target.getAttribute('disabled') === 'disabled') {
            return;
        }

        document.querySelector('#dialog').style.display = 'block';
    }

    function sendRemoveRequest() {
        var contactIds = [];
        var contactTable = document.querySelector('.contacts-table tbody');
        var contactTableRows = contactTable.querySelectorAll('tr');
        for (var i = contactTableRows.length - 1; i >= 0; i--) {
            var box = contactTableRows[i].querySelector('input');
            if (box.checked) {
                var contactId = box.getAttribute('id');
                contactIds.push(contactId.substr(7, contactId.length));
            }
        }
        var jsonIds = JSON.stringify(contactIds);
        sendRequest(contextPath + '/delete', {ids: jsonIds});
    }

    function getSelectedIndex(tableRows) {
        for (var i = 0; i < tableRows.length; i++) {
            if (tableRows[i].querySelector('label.mdl-data-table__select input').checked) {
                return i;
            }
        }
    }

    function prevPage() {
        var pageNum = document.querySelector('button#page-indicator').innerText.trim();
        var numPattern = /^\d+$/;
        if (numPattern.test(pageNum)) {
            sendRequest(location.pathname, {page: --pageNum}, 'GET');
        }
    }

    function nextPage() {
        var pageNum = document.querySelector('button#page-indicator').innerText.trim();
        var numPattern = /^\d+$/;
        if (numPattern.test(pageNum)) {
            sendRequest(location.pathname, {page: ++pageNum}, 'GET');
        }
    }

    function showInputError(inputField) {
        inputField.parentNode.classList.add('is-invalid');
        inputField.parentNode.classList.add('is-dirty');
    }

    function applySearch() {
        var searchModal = document.querySelector('#searchContactsModal');

        var fromDate = searchModal.querySelector('#birthday_from');
        var toDate = searchModal.querySelector('#birthday_to');

        var errorCount = 0;
        if (fromDate.value && !dateRegex.test(fromDate.value)) {
            showInputError(fromDate);
            errorCount++;
        }
        if (toDate.value && !dateRegex.test(toDate.value)) {
            showInputError(toDate);
            errorCount++;
        }
        if (errorCount) {
            return;
        }

        var from;
        var to;
        if (fromDate.value) {
            from = new Date(fromDate.value.split('/')[0], fromDate.value.split('/')[1], fromDate.value.split('/')[2]);
        }
        if (toDate.value) {
            to = new Date(toDate.value.split('/')[0], toDate.value.split('/')[1], toDate.value.split('/')[2]);
        }
        if (from && to) {
            if (from.getTime() > to.getTime()) {
                document.querySelector('#alert_msg').innerText = birthdayErrorMsg;
                document.querySelector('#alert').style.display = 'block';
                return;
            }
        }

        var criteria = {
            address: {}
        };
        if (searchModal.querySelector('input#fname').value) {
            criteria.first_name = searchModal.querySelector('input#fname').value;
        }
        if (searchModal.querySelector('input#lname').value) {
            criteria.last_name = searchModal.querySelector('input#lname').value;
        }
        if (searchModal.querySelector('input#mname').value) {
            criteria.middle_name = searchModal.querySelector('input#mname').value;
        }
        if (fromDate.value) {
            criteria.birthday_from = fromDate.value;
        }
        if (toDate.value) {
            criteria.birthday_to = toDate.value;
        }
        var maritalStatusInput = document.querySelector('input#marital_status').getAttribute('data-val');
        if (maritalStatusInput && maritalStatusInput.toLowerCase() !== 'empty') {
            criteria.marital_status = maritalStatusInput.toLowerCase();
        }
        var sexInput = document.querySelector('input#sex').getAttribute('data-val');
        if (sexInput && sexInput.toLowerCase() !== 'empty') {
            criteria.sex = sexInput.toLowerCase();
        }
        if (searchModal.querySelector('input#nationality').value) {
            criteria.nationality = searchModal.querySelector('input#nationality').value;
        }
        if (searchModal.querySelector('input#country').value) {
            criteria.address.country = searchModal.querySelector('input#country').value;
        }
        if (searchModal.querySelector('input#city').value) {
            criteria.address.city = searchModal.querySelector('input#city').value;
        }
        if (searchModal.querySelector('input#detail_address').value) {
            criteria.address.detail_address = searchModal.querySelector('input#detail_address').value;
        }
        if (searchModal.querySelector('input#zip').value) {
            criteria.address.zip = searchModal.querySelector('input#zip').value;
        }

        var criteriaJson = JSON.stringify(criteria);
        sendRequest(contextPath + '/search', {criteria: criteriaJson});
    }

    function closeModal(event) {
        var searchModal = document.querySelector('#searchContactsModal');
        if (event.target === searchModal) {
            searchModal.style.display = 'none';
        }
    }
}());

function isNumber(event) {
    event = (event) ? event : window.event;
    var charCode = (event.which) ? event.which : event.keyCode;
    return !((charCode > 31 && charCode < 48) || charCode > 57);
}