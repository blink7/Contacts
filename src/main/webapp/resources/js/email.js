(function () {
    'use strict';

    // Fix IE/Edge forEach() error
    if (typeof NodeList.prototype.forEach !== "function") {
        NodeList.prototype.forEach = Array.prototype.forEach;
    }

    var colors = ['F44336', 'E91E63', '9C27B0', '673AB7', '3F51B5', '009688', '607D8B'];

    var chipIdPrefix = 'chip_contact';

    var contextPath = '/contacts';

    function whenLoaded() {
        document.querySelector('#email-btn').addEventListener('click', openEmailBox);
        document.querySelector('#send-btn').addEventListener('click', sendEmail);

        addRadioListener();
        document.querySelector('#email_msg').addEventListener('input', msgBoxListener);
    }

    window.addEventListener ?
        window.addEventListener("load", whenLoaded, false) :
        window.attachEvent && window.attachEvent("onload", whenLoaded);

    function addRadioListener() {
        var msgBox = document.querySelector('#email_msg');
        var templates = document.querySelectorAll('.email-template');
        templates.forEach(function(template) {
            template.querySelector('.mdl-radio__button').addEventListener('change', function() {
                msgBox.value = template.querySelector('.mdl-list__item-text-body').innerText;
                msgBox.parentNode.classList.add('is-dirty');
            });
        });
    }

    function msgBoxListener() {
        var templates = document.querySelectorAll('.email-template');
        templates.forEach(function(template) {
            template.querySelector('.mdl-radio').MaterialRadio.uncheck();
        });
    }

    function openEmailBox(event) {
        if (event.target.getAttribute('disabled') === 'disabled') {
            return;
        }

        var emailModal = document.querySelector('#emailModal');
        var recipientsContainer = emailModal.querySelector('.email-recipients');
        while(recipientsContainer.firstChild) {
            recipientsContainer.removeChild(recipientsContainer.firstChild);
        }

        emailModal.style.display = 'block';

        setTimeout(function() {addRecipients(recipientsContainer)}, 200);
    }

    function addRecipients(recipientsContainer) {
        var contactTableRows = document.querySelectorAll('.contacts-table tbody tr');
        for (var i = contactTableRows.length - 1; i >= 0; i--) {
            var box = contactTableRows[i].querySelector('input');
            if (box.checked) {
                var contactId = box.getAttribute('id');
                var contactColumns = contactTableRows[i].querySelectorAll('td');
                var nameChar = contactColumns[1].innerText.split(' ')[0].charAt(0);
                if (!contactColumns[5]) {
                    continue;
                }
                var email = contactColumns[5].innerText;

                var recipient = document.createElement('span');
                recipient.classList.add('mdl-chip');
                recipient.classList.add('mdl-chip--contact');
                recipient.classList.add('mdl-chip--deletable');
                recipient.classList.add('email-recipient');
                recipient.setAttribute('id', 'chip_' + contactId);

                var circleCell = document.createElement('span');
                circleCell.classList.add('mdl-chip__contact');
                circleCell.style.background = '#' + colors[Math.floor(Math.random() * colors.length)]
                circleCell.classList.add('mdl-color-text--white');
                circleCell.innerText = nameChar;
                recipient.appendChild(circleCell);

                var emailCell = document.createElement('span');
                emailCell.classList.add('mdl-chip__text');
                emailCell.innerText = email;
                recipient.appendChild(emailCell);

                var removeBtn = document.createElement('button');
                removeBtn.classList.add('mdl-chip__action');
                removeBtn.innerHTML = '<i class="material-icons">cancel</i>';
                removeBtn.addEventListener('click', removeRecipient);
                recipient.appendChild(removeBtn);

                recipientsContainer.appendChild(recipient);
            }
        }
    }

    function removeRecipient(event) {
        var recipientsContainer = document.querySelector('.email-recipients');
        recipientsContainer.removeChild(event.target.parentNode.parentNode);
    }

    function sendEmail() {
        var content = {
            recipients: [],
            subject: document.querySelector('#subject').value
        };
        var chosenContacts = document.querySelectorAll('.mdl-chip');
        chosenContacts.forEach(function(contact) {
            var chipId = contact.getAttribute('id');
            content.recipients.push(chipId.substr(chipIdPrefix.length, chipId.length));
        });

        var chosenTemplate;
        var templates = document.querySelectorAll('.email-templates input');
        for (var i = 0; i < templates.length; i++) {
            if (templates[i].checked) {
                chosenTemplate = templates[i].getAttribute('id');
                break;
            }
        }
        if (chosenTemplate) {
            content.template = chosenTemplate;
        } else {
            content.text = document.querySelector('#email_msg').value;
        }
        var contentJson = JSON.stringify(content);

        if (content.recipients.length) {
            sendRequest(contextPath + '/send', {content: contentJson});
        } else {
            document.querySelector('#alert_msg').innerText = mailErrorMsg;
            document.querySelector('#alert').style.display = 'block';
        }
    }
}());