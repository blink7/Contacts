(function () {
	'use strict';

	function whenLoaded() {
		document.querySelector('#back-btn').addEventListener('click', function() {
            location.href='/contacts';
        });
	}

	window.addEventListener ?
		window.addEventListener("load", whenLoaded, false) :
		window.attachEvent && window.attachEvent("onload", whenLoaded);
}());