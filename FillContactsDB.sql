INSERT INTO blink7_contacts.address(country, city, detail_address, zip) VALUES('Belarus', 'Minsk', 'Rusijanava, 1, 25', 125125);
INSERT INTO blink7_contacts.address(country, city, detail_address, zip) VALUES('Belarus', 'Minsk', 'Nikifarava, 2, 24', 224224);
INSERT INTO blink7_contacts.address(country, city, detail_address, zip) VALUES('Belarus', 'Minsk', 'Suhajeva, 3, 23', 323323);
INSERT INTO blink7_contacts.address(country, city, detail_address, zip) VALUES('Belarus', 'Minsk', 'Lozynskaja, 4, 22', 422422);
INSERT INTO blink7_contacts.address(country, city, detail_address, zip) VALUES('Belarus', 'Minsk', 'Niezalieznasci, 5, 21', 521421);
INSERT INTO blink7_contacts.address(country, city, detail_address, zip) VALUES('Belarus', 'Minsk', 'Astrasyckaja, 6, 20', 620620);
INSERT INTO blink7_contacts.address(country, city, detail_address, zip) VALUES('Belarus', 'Minsk', 'Starynauskaja, 7, 19', 719719);
INSERT INTO blink7_contacts.address(country, city, detail_address, zip) VALUES('Belarus', 'Minsk', 'Pastovaja, 8, 18', 818818);
INSERT INTO blink7_contacts.address(country, city, detail_address, zip) VALUES('Belarus', 'Minsk', 'Francyska Skaryny, 9, 17', 917917);
INSERT INTO blink7_contacts.address(country, city, detail_address, zip) VALUES('Belarus', 'Minsk', 'Hintauta, 10, 16', 101610);
INSERT INTO blink7_contacts.address(country, city, detail_address, zip) VALUES('Belarus', 'Minsk', 'Niabiesnaja, 11, 15', 111511);
INSERT INTO blink7_contacts.address(country, city, detail_address, zip) VALUES('Belarus', 'Minsk', 'Haradzieckaja, 12, 14', 121412);
INSERT INTO blink7_contacts.address(country, city, detail_address, zip) VALUES('Belarus', 'Minsk', 'Rusijanava, 13, 13', 131313);
INSERT INTO blink7_contacts.address(country, city, detail_address, zip) VALUES('Belarus', 'Minsk', 'Nikifarava, 14, 12', 141214);

INSERT INTO blink7_contacts.contact(f_name, l_name, birthday, sex, marital_status, address_id) 
VALUES ('Ivan', 'Ivanov', '1989/01/01', 'male', 'single', 1);
INSERT INTO blink7_contacts.contact(f_name, l_name, birthday, sex, marital_status, address_id) 
VALUES ('Semen', 'Petrov', '1990/02/12', 'male', 'single', 2);
INSERT INTO blink7_contacts.contact(f_name, l_name, birthday, sex, marital_status, address_id) 
VALUES ('Gera', 'Somov', '1990/05/10', 'male', 'single', 3);
INSERT INTO blink7_contacts.contact(f_name, l_name, birthday, sex, marital_status, address_id) 
VALUES ('Vasil', 'Dremov', '1990/03/22', 'male', 'single', 4);
INSERT INTO blink7_contacts.contact(f_name, l_name, birthday, sex, marital_status, address_id) 
VALUES ('Boris', 'Mohov', '1990/03/20', 'male', 'single', 5);
INSERT INTO blink7_contacts.contact(f_name, l_name, birthday, sex, marital_status, address_id) 
VALUES ('Zema', 'Hotic', '1990/06/21', 'male', 'single', 6);
INSERT INTO blink7_contacts.contact(f_name, l_name, birthday, sex, marital_status, address_id) 
VALUES ('Farid', 'Faidulov', '1990/03/20', 'male', 'single', 7);
INSERT INTO blink7_contacts.contact(f_name, l_name, birthday, sex, marital_status, address_id) 
VALUES ('Alex', 'Sadov', '1990/06/21', 'male', 'single', 8);
INSERT INTO blink7_contacts.contact(f_name, l_name, birthday, sex, marital_status, address_id) 
VALUES ('Masha', 'Petrova', '1990/03/11', 'female', 'single', 9);
INSERT INTO blink7_contacts.contact(f_name, l_name, birthday, sex, marital_status, address_id) 
VALUES ('Dasha', 'Petrova', '1990/03/11', 'female', 'single', 10);
INSERT INTO blink7_contacts.contact(f_name, l_name, birthday, sex, marital_status, address_id) 
VALUES ('Dasha', 'Vasilieva', '1990/04/21', 'female', 'single', 11);
INSERT INTO blink7_contacts.contact(f_name, l_name, birthday, sex, marital_status, address_id) 
VALUES ('Maria', 'Dutova', '1990/08/12', 'female', 'single', 12);
INSERT INTO blink7_contacts.contact(f_name, l_name, birthday, sex, marital_status, address_id) 
VALUES ('Vasilisa', 'Premudraja', '1990/09/17', 'female', 'single', 13);
INSERT INTO blink7_contacts.contact(f_name, l_name, birthday, sex, marital_status, address_id) 
VALUES ('Natasha', 'Rochina', '1990/09/17', 'female', 'single', 14);

INSERT INTO blink7_contacts.phone(contact_id, country_code, operator_code, phone_number, phone_type, comment_text) 
VALUES(1, 375, 25, 2359482, 'mobile', 'It\'s my main phone');
INSERT INTO blink7_contacts.phone(contact_id, country_code, operator_code, phone_number, phone_type) 
VALUES(1, 375, 17, 6544432, 'home');